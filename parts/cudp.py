#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 21:39:01 2017

@author: thomas
"""
import socket


class UDPClient():
    def __init__(self,HOST,PORT):
        self.HOST = HOST
        self.PORT = PORT
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    def send(self,data):
        self.sock.sendto(bytes(data + "\n", "utf-8"), (self.HOST, self.PORT))
        received = str(self.sock.recv(1024), "utf-8")
        print("Sent:     {}".format(data))
        print("Received: {}".format(received))

client = UDPClient("localhost",9999)
client.send("bahaha")
