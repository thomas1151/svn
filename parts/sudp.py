#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 21:38:31 2017

@author: thomas
"""
import socket
import socketserver

class UDPHandler(socketserver.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        self.data = self.request[0].strip()
        self.socket = self.request[1]
        print("{} wrote:".format(self.client_address[0]))
        print(self.data)
        self.socket.sendto(self.data.upper(), self.client_address)


    # Create the server, binding to localhost on port 9999

def run(HOST,PORT):

    print("socket running")
    server = socketserver.UDPServer((HOST, PORT), UDPHandler)
    server.serve_forever()
    # Activate the server; this will keep running until you
    #interrupt the program with Ctrl-C


run("localhost",9999)