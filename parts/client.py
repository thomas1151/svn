#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 14:16:12 2017

@author: thomas
"""

import socket
import sys
import multiprocessing
import kivy
kivy.require('1.0.6') # replace with your current kivy version !

from random import random
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.graphics import Color, Ellipse, Line


class MyPaintWidget(Widget):

    def on_touch_down(self, touch):
        color = (random(), 1, 1)
        with self.canvas:
            Color(*color, mode='hsv')
            d = 30.
            Ellipse(pos=(touch.x - d / 2, touch.y - d / 2), size=(d, d))
            touch.ud['line'] = Line(points=(touch.x, touch.y),width=20)

    def on_touch_move(self, touch):
        touch.ud['line'].points += [touch.x, touch.y]


class MyPaintApp(App):

    def build(self):
        parent = Widget()
        self.painter = MyPaintWidget()
        clearbtn = Button(text='Clear')
        clearbtn.bind(on_release=self.clear_canvas)
        parent.add_widget(self.painter)
        parent.add_widget(clearbtn)
        return parent

    def clear_canvas(self, obj):
        self.painter.canvas.clear()


if __name__ == '__main__':
    MyPaintApp().run()

class Client():
    # Create a socket (SOCK_STREAM means a TCP socket)
   def __init___(self,host,port):
       self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
       self.host,self.port = host,port
   def send(self,data):    
    try:
        # Connect to server and send data
        self.sock.connect((self.host, self.port))
        self.sock.sendall(bytes(data + "\n", "utf-8"))
    
        # Receive data from the server and shut down
        self.received = str(self.sock.recv(1024), "utf-8")
    finally:
        self.sock.close()

   def log_send(self,data,received):
        print("Sent:     {}".format(data))
        print("Received: {}".format(received))
    

#if __name__ == '__main__':
#    jobs = []
#    p = multiprocessing.Process(target=Client())
#    jobs.append(p)
#    p.start()

data = "Hello from the other side "
