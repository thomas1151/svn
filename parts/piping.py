#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  9 22:01:25 2017

@author: thomas
"""

'''capture.py'''
import cv2, sys
import numpy as np
cap = cv2.VideoCapture(0)                    # 0 is for /dev/video0
while True :
    ret, frm = cap.read()
    sys.stdout.write( np.array2string(np.array(frm.tostring)))