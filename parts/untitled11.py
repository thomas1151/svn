#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  3 14:23:24 2017

@author: thomas
"""
import urllib.request as ul

class NoRedirectHandler(ul.HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        infourl = ul.addinfourl(fp, headers, req.get_full_url())
        infourl.status = code
        infourl.code = code
        return infourl
    http_error_300 = http_error_302
    http_error_301 = http_error_302
    http_error_303 = http_error_302
    http_error_307 = http_error_302

opener = ul.build_opener(NoRedirectHandler())
ul.install_opener(opener)
response = ul.urlopen('http://www.binarytides.com/programming-udp-sockets-in-python/')
if response.code in (300, 301, 302, 303, 307):
    print('redirect')
