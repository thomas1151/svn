#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 20:49:32 2017

@author: thomas
"""
import socket
import sys
from threading import Thread
from pickle import dumps,loads
import numpy as np
import cv2
import random 
import os
import shutil

class SocketHandler():
    def listen():
        UDPSock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

        # Listen on port 21567
        # (to all IP addresses on this system)
        listen_addr = ("",6800)
        UDPSock.bind(listen_addr)

        # Report on all data packets received and
        # where they came from in each case (as this is
        # UDP, each may be from a different source and it's
        # up to the server to sort this out!)
        

        i =0
        currentFrame = -1
        fpsf=0
        fps = []
        x = []
        y = []
        rows = []

        while True:
                data,addr = UDPSock.recvfrom(1000000)
#                print(data, addr)
                data = loads(data)
#                print(type(data))
#                print('\n \n \n \n',data)
 #               print(data['data'])
                #print(data['type'])

                if data['type'] == 'vframe':
                    print(data['meta']['fp-id'],data['meta']['coords'],';',data['meta']['shape'])
                    #fpsf = 0 #Frame parts so far

                    if data['meta']['id'] == currentFrame:
                        try:
                            print("adding")
                            fps.append(data['data'])
                            
                        except IndexError:
                            pass
                        fpsf+=1
                    else:
                        
                        #Failing on first!
                        
                        
                        
                        
                        
                        
                        if currentFrame < 0:
                            pass
                        fpsf=0
                        
                        currentFrame = data['meta']['id']
                        #frame = np.zeros(data['meta']['fs'])
                            #frame = cv2.imwrite('frames/tmp/img'+str(i)+'.png', frame)
                        l = 0
                        t = 0
#                            print(data['meta']['fp-id'][1])
#
#                            while t < data['meta']['fp-id'][1]:
#                                rows.append([])
#                                while l < data['meta']['fp-id'][0]:
#                                    rows[-1].append(fps[l])
#                                    l+=1
#                                t+=1
#                            
                        fr = np.hstack(np.array(fps))
                        #fr = np.concatenate(np.array(fps), axis=(0,1))
                        #print(fr)
                        #fr = np.squeeze(np.array(np.concatenate((*fps))))
                        #print(fr)
                        #print(rows)
                        #cv2.imshow('ba',rows)
                        frame = cv2.imwrite('frames/tmp/'+str(i)+'.png',fr)
                        fps = []
                            
                    #print(frame.shape)
#                    frame = np.fromstring(data['data'],dtype=data['meta']['dtype'])
#                    print(len(frame))
#                    frame = frame.reshape(eval(data['meta']['shape']))
#                    frame = cv2.imwrite('frames/tmp/'+str(frameServerID)+'/'+'FRAMEPARTID:'+str(data['meta']['fp-id'])+'COORDS..'+str(data['meta']['coords'])+'img'+'.png', frame)
                    i+=1
                

    def receive():
        # create dgram udp socket
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except socket.error:
            print('Failed to create socket')
            sys.exit()

        host = 'localhost'
        port = 6789

        while(1) :
            msg = 'Enter message to send : '

            try :
                #Set the whole string
                s.sendto(msg, (host, port))

                # receive data from client (data, addr)
                d = s.recvfrom(1024)
                reply, addr = d
                print('Server reply : ' + reply)

            except socket.error as msg:
                print('Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
                sys.exit()

    def open(portNo,listenNo):
        '''
        Port Number refers to which port the client tunnel should be.
        Listener port remains constant at port 6800.
        '''


        listener_thread = Thread(target=SocketHandler.listen)
        listener_thread.start()

    def close():
        return

SocketHandler.open(6400,10000)