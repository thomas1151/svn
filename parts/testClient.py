#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 30 10:06:10 2017

@author: thomas
"""

import socket
import random
import time
import cv2
import numpy as np
import binascii
from pickle import dumps, loads
import time


data = cv2.imread("frame.png")
#print(data)
#print(data.dtype)
UDP_IP = "10.30.0.11"
UDP_PORT = 6800
#
#MESSAGE = ("Hello, World!").encode()
#
#sock = socket.socket(socket.AF_INET, # Internet
#                     socket.SOCK_DGRAM) # UDP
#sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))
#i=0

class Sender():
    def __init__(self,host_ip,host_port):
        self.sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
        self.host_ip = host_ip
        self.host_port = host_port
    def send(self,message):
        self.sock.sendto(message,(self.host_ip,self.host_port))
        
frame = {
     "type":'message',
     "meta":{},
     "data":'blaaah', 
     
     }
     
#
#r = 199 / data.shape[1]
#dim = (199, int(data.shape[0] * r))
#comp= cv2.resize(data, dim, interpolation = cv2.INTER_AREA)
#dframe = {
#    "type":"vframe",
#    "meta":{'name':'Frame','dtype':'uint8','shape':str(comp.shape),'len':len(comp)},
#    "data":comp
#    }
     

cap=cv2.VideoCapture("sample.mp4")
frameID = 0
while(cap.isOpened()):
    ret, frame = cap.read()

    if frame is None:
        break
    #print(type(frame))        
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    height, width = frame.shape[:2]
    comp = cv2.resize(frame,(int(.26*width), int(.26*height)), interpolation = cv2.INTER_CUBIC)
    n = True
    xdivisions = 8
    ydivisions = 8
    currentdivision = [0,0]
    divisionlength  = [int(gray.shape[0]*(xdivisions)**-1),int(gray.shape[1]*(ydivisions)**-1)]
    print(divisionlength)
    #print(divisionlength)
    startx =  0   #Row to srart division on
    starty = 0
    route = Sender('127.0.0.1',6800)
    framecomp = (0,0,3) #Indicating starting pos of 0, but with 3 color channels
    
    while currentdivision[0]*divisionlength[0] <= frame.shape[0]-divisionlength[0]: #or currentdivision[1]*divisionlength[1] < frame.shape[1] :
#    while startx+divisionlength[0] < frame.shape[0]+divisionlength[0] or starty+divisionlength[1] < frame.shape[1]+divisionlength[1]:

    #while currentdivision[0]<=xdivisions and currentdivision[1]<=ydivisions:
        #starty = 0
        #print(currentdivision[0]*divisionlength[0],frame.shape)
        time.sleep(.018)
        #print(frame.shape,[startx+divisionlength[0],starty+divisionlength[1]])
        while currentdivision[1]*divisionlength[1]<= frame.shape[1]-divisionlength[1]:
            print((currentdivision[0]*divisionlength[0],currentdivision[1]*divisionlength[1]),frame.shape)

            #currentdivision[1]*divisionlength[1]+currentdivision[1]*divisionlength[1]-((currentdivision[1]-1)*divisionlength[1]
            time.sleep(.018)
            #framecomp = frame[currentdivision[0]*divisionlength[0]:currentdivision[0]*2*divisionlength[0],currentdivision[1]*divisionlength[1]:currentdivision[1]*2*divisionlength[1]]
            framecomp =  frame[divisionlength[0]*currentdivision[0]:divisionlength[0]*(currentdivision[0]+1),divisionlength[1]*currentdivision[1]:divisionlength[1]*(currentdivision[1]+1)]
#
#            print('Current Division starts at:',currentdivision[1]*divisionlength[1])
#            print('Division length: ',divisionlength[1])
#            print('Range then:',[divisionlength[1]*currentdivision[1],divisionlength[1]*(currentdivision[1]+1)]    )
            print('Shape',framecomp.shape)
#            print('\n \n \n')
            currentdivision[1] +=1
            #print("framesec",framecomp.shape)
            dframe = {
                "type":"vframe",
                "meta":{'fs':frame.shape,'id':frameID,'fp-id':currentdivision,'dtype':'uint8','coords':[startx,starty],'shape':str(framecomp.shape),'len':len(framecomp)},
                "data":framecomp
            }
            route.send(dumps(dframe))
            #currentdivision[1]+=1
            #print(currentdivision,startx,starty)

        currentdivision[1] = 0
#
        #framecomp = frame[startx:startx+divisionlength[0],starty:starty+divisionlength[1]]
        #startx = startx+divisionlength[0]
         #amount of rows one division will take
        #print("framesec",framecomp.shape)
        
#        dframe = {
#            "type":"vframe",
#            "meta":{'div-length':divisionlength,'fs':frame.shape,'id':frameID,'fp-id':currentdivision,'dtype':'uint8','coords':[startx,starty],'shape':str(framecomp.shape),'len':len(framecomp)},
#            "data":framecomp
#        }
#    
#        route.send(dumps(dframe))
        #print(currentdivision,startx,starty)
        #print('Total',currentdivision[0]*currentdivision[1])
        currentdivision[0]+=1
    frameID +=1

    dframe = {
        "type":"vframe",
        "meta":{'div-length':divisionlength,'fs':frame.shape,'id':frameID+2,'fp-id':currentdivision,'dtype':'uint8','coords':[startx,starty],'shape':str(framecomp.shape),'len':len(framecomp)},
        "data":framecomp
    }

    route.send(dumps(dframe))    
    break

    #
#            

#print(comp.shape)


