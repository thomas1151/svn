#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 31 20:39:56 2017

@author: thomas
"""

import numpy as np
import cv2
import inspect
cap=cv2.VideoCapture("sample.mp4")

while(cap.isOpened()):
    ret, frame = cap.read()

    if frame is None:
        break
    print(type(frame))        
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

