#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 13 12:55:07 2017

@author: thomas
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 13:04:35 2016

@author: Matthew
"""

import sys
import math

from PyQt4.QtCore import *
from PyQt4 import QtGui, QtCore

from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas

class UI(QtGui.QWidget):    
    
    def __init__(self):
        super(UI, self).__init__()
        QtGui.QWidget.__init__(self, None)
        
        self.show()
        
        self.mainLayout = QtGui.QVBoxLayout(self)
        self.grid = QtGui.QGridLayout()
        
        self.graph = ProjGraph()
        
        self.angleLabel = QtGui.QLabel("Angle (ᵒ)")
        self.speedLabel = QtGui.QLabel("Velocity (ms^-1)")
        self.heightLabel = QtGui.QLabel("Height (m)")
        
        self.angleBox = QtGui.QLineEdit()
        self.speedBox = QtGui.QLineEdit()
        self.heightBox = QtGui.QLineEdit()
        
        self.button = QtGui.QPushButton("Plot")
        
        self.button.clicked.connect(lambda: self.buttonPress(self.angleBox.text(),
                                                             self.speedBox.text(),
                                                             self.heightBox.text()))
        
        self.grid.addWidget(self.graph,0,0,8,4)
        
        self.grid.addWidget(self.angleLabel,9,0,1,1)
        self.grid.addWidget(self.speedLabel,9,1,1,1)
        self.grid.addWidget(self.heightLabel,9,2,1,1)
        
        self.grid.addWidget(self.angleBox,10,0,1,1)
        self.grid.addWidget(self.speedBox,10,1,1,1)
        self.grid.addWidget(self.heightBox,10,2,1,1)
        
        self.grid.addWidget(self.button,10,3,1,1)
        
        self.grid.setSpacing(10)
        
        self.mainLayout.addLayout(self.grid)
        self.mainLayout.addStretch()
        
        self.invalidError = QtGui.QErrorMessage()
        self.invalidError.setWindowTitle("Error")
        
        self.plotThread = PlotThread()
        self.plotThread.plot.connect(self.drawData)
                           
    @QtCore.pyqtSlot()
    def buttonPress(self,angle,speed,height):
        try:
            self.plotThread.setFields(float(angle),float(speed),float(height))
            self.graph.axes.clear()
            self.plotThread.start()
        except ValueError:            
            self.invalidError.showMessage("Invalid Values")
        
    @QtCore.pyqtSlot(list,list)
    def drawData(self,x,y):
        self.graph.axes.plot(x,y)
        self.graph.axes.set_ylim(0,max(y)*1.1)
        self.graph.canvas.draw()
            
class PlotThread(QtCore.QThread):
    plot = QtCore.pyqtSignal(list,list)
    
    def __init__(self,parent = None):
        super(PlotThread, self).__init__(parent)
        
    def run(self):
        radAngle = math.radians(self.angle)
        
        vertVelocity = math.sin(radAngle)*self.speed
        horiVelocity = math.cos(radAngle)*self.speed
        
        arcTime = (2*vertVelocity)/9.81
        
        finalVelocity = math.sqrt((vertVelocity**2)+(2*9.81*self.height))
        
        dropTime = (finalVelocity-vertVelocity)/9.81        
        
        totalTime = arcTime + dropTime
        
        timePoints = []
        for i in range(51):
            timePoints.append((totalTime/50)*i)
            
        yPoints = []
        for t in timePoints:
            yPoints.append((t*(vertVelocity+(-9.81*t*0.5)))+self.height)
            
        xPoints = []
        for t in timePoints:
            xPoints.append(horiVelocity*t)
                
        self.plot.emit(xPoints,yPoints)
        
    def setFields(self,angle,speed,height):
        self.angle = angle
        self.speed = speed
        self.height = height
            
class ProjGraph(QtGui.QWidget):
    def __init__(self, parent = None):
        super(ProjGraph,self).__init__(parent)        
        
        self.fig = Figure(figsize=(15,7))
        self.canvas = FigureCanvas(self.fig)
        
        self.axes = self.fig.add_subplot(111)
              
        self.layoutVertical = QtGui.QVBoxLayout(self)
        self.layoutVertical.addWidget(self.canvas)

def main():
    
    app = QtGui.QApplication(sys.argv)
    widget = UI()
    widget.setFixedSize(800,600)
    sys.exit(app.exec_())
    
if __name__ == "__main__":
    main()