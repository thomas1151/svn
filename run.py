import os
from tornado.options import options, define, parse_command_line
import tornado.httpserver
import tornado.ioloop
import tornado.websocket
import tornado.wsgi
import tornado.web
from tornado import web, gen, netutil, ioloop, iostream, httpserver


import random
import string
import socket
import json
import numpy as np
import cv2
import binascii
from pickle import dumps,loads
os.environ['DJANGO_SETTINGS_MODULE'] = 'svn.settings' # path to your settings module
import django
django.setup()



from django.core.wsgi import get_wsgi_application
import django.core.handlers.wsgi
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from p2pshare.models import System,ClientOption,ConnectionType
from threading import Thread



class SystemControl():
    def __init__(self):
        self.cl = []
        #List of Options that the client can send: Convert this to django model I feel
        self.responses = {'type':'OPTIONS','options'    :[]}
        for item in ClientOption.objects.all():
            toJSON = {'id':item.id,'name':item.name,'description':item.description}
            self.responses['options'].append(toJSON)
        print(self.responses)
        # self.responses = {
        #     'type':'OPTIONS',
        #     'options':[
        #         {'name':'openSocket','details':{'id':0,'description':'Open the socket on port'}},
        #         {'name':'closeSocket','details':{'id':1,'description':'Close the socket on port'}}
        #     ]
        # }
        self.commandI = [WSHandler.openSocket,WSHandler.closeSocket]
    def cprint(self,data):
        for i in self.cl:
            i.write_message(data)


    def cmessage(self,data):
        message = {
            'type':'MESSAGE',
            'message':str(data),
        }
        self.cprint(message)


    def prange(mina,maxa):
        data = []
        for i in range(mina,maxa):
            print(i)
            data.append(i)
        return data

    def connection_ready(sock, fd, events):
        while True:
            try:
                connection, address = sock.accept()
            except socket.error as e:
                if e[0] not in (errno.EWOULDBLOCK, errno.EAGAIN):
                    raise
                return
            else:
                connection.setblocking(0)
                CommunicationHandler(connection)

class Connection(object):
    def __init__(self, connection):
        self.stream = iostream.IOStream(connection)
        self._read()

    def _read(self):
        self.stream.read_until('\r\n', self._eol_callback)

    def _eol_callback(self, data):
        self.handle_data(data)

class CommunicationHandler(Connection):
    """Put your app logic here"""
    def handle_data(self, data):
        self.stream.write(data)
        self._read()

import asyncio

class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        print('Send %r to %s' % (message, addr))
        self.transport.sendto(data, addr)


class SocketHandler():

    def listen():
        SocketHandler.UDPSock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)

        # Listen on port 21567
        # (to all IP addresses on this system)
        listen_addr = ("",6800)
        SocketHandler.UDPSock.bind(listen_addr)

        # Report on all data packets received and
        # where they came from in each case (as this is
        # UDP, each may be from a different source and it's
        # up to the server to sort this out!)
        syst.cmessage("Listening on "+str(6800))
        while True:
                data,addr = SocketHandler.UDPSock.recvfrom(1000000)
                print(data, addr)
                data = loads(data)
                print(type(data))
                syst.cmessage(data)
                print('\n \n \n \n',data)
                print(data['data'])
                if data['type'] == 'vframe':
                    syst.cmessage("Video Frame Received")
                    frame = np.fromstring(data['data'],dtype=data['meta']['dtype'])
                    print(len(frame))
                    frame = frame.reshape(eval(data['meta']['shape']))
                    frame = cv2.imwrite('color_img.png', frame)

    def receive():
        # create dgram udp socket
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except socket.error:
            print('Failed to create socket')
            sys.exit()

        host = 'localhost'
        port = 6789

        while(1) :
            msg = 'Enter message to send : '

            try :
                #Set the whole string
                s.sendto(msg, (host, port))

                # receive data from client (data, addr)
                d = s.recvfrom(1024)
                reply, addr = d
                print('Server reply : ' + reply)

            except socket.error as msg:
                print('Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
                sys.exit()

    def open(portNo,listenNo):
        '''
        Port Number refers to which port the client tunnel should be.
        Listener port remains constant at port 6800.
        '''


        listener_thread = Thread(target=SocketHandler.listen)
        listener_thread.start()

        # SocketHandler.listenerSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        # SocketHandler.listenerSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        #
        # SocketHandler.clientSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        # SocketHandler.clientSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # SocketHandler.listenerSock.setblocking(0)
        # SocketHandler.clientSock.setblocking(0)
        # try:
        #     SocketHandler.listenerSock.bind(("", 6800))
        #     SocketHandler.clientSock.bind(("", portNo))
        #
        # except OSError:
        #     syst.cmessage("Socket is already open!")
        #
        # SocketHandler.listenerSock.listen(5)
        # SocketHandler.clientSock.listen(listenNo)
        #
        # while True:
        #     try:
        #         conn, addr = SocketHandler.listenerSock.accept()
        #         data= conn.recv(1024)
        #     except BlockingIOError:
        #         data = "0"
        #         pass
        #     if len(data) > 1:
        #         syst.cmessage(data)
    def close():
        try:
            SocketHandler.UDPSock.shutdown(socket.SHUT_RDWR)
            SocketHandler.UDPSock.close()

        except OSError:
            pass
            syst.cmessage("Can't close an open socket ")


class WSHandler(tornado.websocket.WebSocketHandler):
    def open(self):
        print('new connection')
        syst.cl.append(self)
        syst.cmessage("GUI Connection is successful")
        syst.cprint(syst.responses)

    def on_message(self, message):

        print("message",message)
        data = json.loads(message)
        print()
        eval(ClientOption.objects.get(id=data['id']).function+'()')
        syst.cprint(syst.responses)

        # for i in syst.cl:
        #     i.write_message(message)
        #
    def on_close(self):
        if self in syst.cl:
            syst.cl.remove(self)
            SocketHandler.close()
    def openSocket(portNo=6789,listenNo=128):
        print("openSocket received!")
        SocketHandler.open(6789,128)


        systemDB = System.objects.get(id=1)
        systemDB.connected = ConnectionType.objects.get(code='a')
        systemDB.save()
        print(System.objects.get(id=1).connected)
        syst.cmessage("Socket listening @ "+str(portNo))
    def closeSocket():
        socketNo = str(SocketHandler.UDPSock.getsockname()[1])
        SocketHandler.close()

        systemDB = System.objects.get(id=1)
        systemDB.connected = ConnectionType.objects.get(code='n')
        systemDB.save()
        print(System.objects.get(id=1).connected)

        syst.cmessage("Socket on "+socketNo+" has been closed")

    def testConnection():
        print()
        return


class rangeHandler(web.RequestHandler):
    @web.asynchronous
    def get(self,*args):
        self.finish()
        print("Gotten")
        print(syst.cl)
        socketInit(6789)
        for i in range(0,10):
             if i == 4:
                 syst.cl[0].write_message(str(i))

def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "svn.settings")
    django.setup()
    define('port', type=int, default=8800)
    settings = {
        "static_path": os.path.join(os.path.dirname(__file__), "static"),
    }
    wsgi_app = tornado.wsgi.WSGIContainer(
        django.core.handlers.wsgi.WSGIHandler()
    )

    tornado_app = tornado.web.Application(
    [
      ('/s/', SocketHandler),
      (r'/ws/',WSHandler),
      ('/range',rangeHandler),
      (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': os.path.dirname(os.path.realpath(__file__))+"/static/"}),
      ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),


      ])
    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    syst = SystemControl()
    main()
