from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
import socket
import socket
import socketserver


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'svn.settings')
app = Celery('p2pshare')
app.conf.broker_url = 'redis://localhost:6379'

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


class UDPHandler(socketserver.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        self.data = self.request[0].strip()
        self.socket = self.request[1]
        print("{} wrote:".format(self.client_address[0]))
        print(self.data)
        self.socket.sendto(self.data.upper(), self.client_address)


    # Create the server, binding to localhost on port 9999
@app.task
def run(HOST,PORT):

    print("socket running")
    server = socketserver.UDPServer((HOST, PORT), UDPHandler)
    server.serve_forever()
    # Activate the server; this will keep running until you
    #interrupt the program with Ctrl-C



class UDPClient():
    def __init__(self,HOST,PORT):
        self.HOST = HOST
        self.PORT = PORT
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
@app.task
def send(client,data):
    client.sock.sendto(bytes(data + "\n", "utf-8"), (client.HOST, client.PORT))
    received = str(client.sock.recv(1024), "utf-8")
    print("Sent:     {}".format(data))
    print("Received: {}".format(received))

client = UDPClient("localhost",9999)
send.delay(client,"ahaha")

# client.send("bahaha")
# run("localhost",9999)
