from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from p2pshare.serializers import UserSerializer, GroupSerializer, PeerSerializer, VideoSerializer, SystemSerializer
from django.http import HttpResponse
from django.views import View
from p2pshare.models import Peer, Video, System
#import socket
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

#This project is going to require 3 sockets
#OUTGOING RECEPTION     (6781?)
#OUTGOING EXPRESSION    (6782?)
#INTERNAL DISPLAY       (8000?)

def index(request):
    return HttpResponse("Hello, world. You're at the share index.")


def detail(request, poll_id):
    try:
        p = Poll.objects.get(pk=poll_id)
    except Poll.DoesNotExist:
        raise Http404("Poll does not exist")
    return render(request, 'polls/detail.html', {'poll': p})

class SystemManage(APIView):
    """
    List system data, or update system settings.
    """
    def get(self, request, format=None):
        system = System.objects.all()
        serializer = SystemSerializer(system, many=True)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        system = self.get_object(pk)
        serializer = SystemSerializer(system, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ConnectionView(View):
    template_folder = 'connection'
    view_vars = {'name':'Connection'}
    def connect(self,request,*args,**kwargs):
        return

    def get(self, request, *args, **kwargs):
        return render(request, self.template_folder+'/get.html',                {
                                                                                'name':'Manage Connection'
                                                                                })

    def post(self, request, *args, **kwargs):

        return render(request, self.template_name, {'form': form})


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class PeerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Peer.objects.all()
    serializer_class = PeerSerializer


class SystemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = System.objects.all()
    serializer_class = SystemSerializer

class VideoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
