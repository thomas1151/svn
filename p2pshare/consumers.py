from django.http import HttpResponse
from channels.handler import AsgiHandler
import redis
from django.dispatch import receiver
import django.dispatch
import socket
import sys


import socketserver

class UDPHandler(socketserver.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]
        print("{} wrote:".format(self.client_address[0]))
        print(data)
        socket.sendto(data.upper(), self.client_address)

    def __init__(self,HOST,PORT):
        self.HOST = HOST
        self.PORT = PORT

    # Create the server, binding to localhost on port 9999

    def run(self):
        print("socket running")
        server = socketserver.UDPServer((self.HOST, self.PORT), UDPHandler)
        server.serve_forever()
        # Activate the server; this will keep running until you
        #interrupt the program with Ctrl-C
        server.serve_forever()

class UDPClient():
    def __init__(self,HOST,PORT):
        self.HOST = HOST
        self.PORT = PORT
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    def send(self,data):
        sock.sendto(bytes(data + "\n", "utf-8"), (self.HOST, self.PORT))
        received = str(sock.recv(1024), "utf-8")
        print("Sent:     {}".format(data))
        print("Received: {}".format(received))





# SOCK_DGRAM is the socket type to use for UDP sockets

# As you can see, there is no connect() call; UDP has no connections.
# Instead, data is directly sent to the recipient via sendto().



#Clients need to be made aware when:
#Socket is opened
#Connection to socket begins
#Result of socket Connection
#
class socketStore(object):
#
     def send_socketState(self, toppings, size):
        pizza_done.send(sender=self.__class__, toppings=toppings, size=size)
#
#
#
# # In consumers.py
redis_conn = redis.Redis("localhost", 6379)
#
# #@receiver(post_save, sender=BlogUpdate)
# def send_update(sender, instance, **kwargs):
#     # Loop through all reply channels and send the update
#     for reply_channel in redis_conn.smembers("readers"):
#         Channel(reply_channel).send({
#             "text": json.dumps({
#                 "id": instance.id,
#                 "content": instance.content
#             })
#         })


def prange(mina,maxa):
    data = []
    for i in range(mina,maxa):
        print(i)
        data.append(i)
    return data
def openSocket():
    return

def ws_message(message):
    # ASGI WebSocket packet-received and send-packet message types
    # both have a "text" key for their textual data.
    if message.content['text'] == "beginSending":
        client = UDPClient("localhost",9998)
        server = UDPHandler("localhost", 9998)
        server.run()
        client.send("blahaha")
        data = prange(0,200)
        for i in data:
            message.reply_channel.send({
                # "text": message.content['text'],
                "text": str(i),
            })

    else:
        message.reply_channel.send({"text":""})


def ws_sendCommand(message,output):
    message.reply_channel.send({"text":str(output)})

# Connected to websocket.connect
def ws_add(message):
    # Add to reader set
    redis_conn.sadd("readers", message.reply_channel.name)
    ws_sendCommand(message,"Added to reader set")
