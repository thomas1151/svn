from channels.routing import route
from p2pshare.consumers import ws_message, ws_add

channel_routing = [
    route("websocket.receive", ws_message), #Upon being sent something by a user.
    route("websocket.connect", ws_add),
]
