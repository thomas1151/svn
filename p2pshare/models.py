from django.db import models

# Create your models here.
class ConnectionType(models.Model):
    connection_types = (
        ('e', 'Expression' ), #Sending Data
        ('r', 'Reception'),   #Receiving Data
        ('a', 'Attempting Connection'), #Not likely used in this build.
        ('n', 'No Connection'), #No Connection

    )
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=1,choices=connection_types)


class System(models.Model):

    connected = models.ForeignKey(ConnectionType)
    peer_ip   = models.CharField(max_length=20,default=0)

class Peer(models.Model):
    peer_types = (
        ('r', 'Receptive Peer' ),
        ('e', 'Expressive Peer'),
        ('s', 'Self')
    )
    peer_type   = models.CharField(max_length=1,choices=peer_types)
    username    = models.CharField(max_length=30)
    display_name= models.CharField(max_length=30)
    public_key  = models.CharField(max_length=30)
    secret_key  = models.CharField(max_length=30)
    password    = models.CharField(max_length=50)

class Video(models.Model):
    store_date  = models.DateTimeField()
    store_length= models.IntegerField()
    length      = models.IntegerField()
    name        = models.CharField(max_length=100)
    uploader_id = models.CharField(max_length=200)
    x_resolution= models.IntegerField()
    y_resolution= models.IntegerField()

class Block(models.Model):
    video_id        = models.ForeignKey(Video, on_delete=models.CASCADE)
    location        = models.FileField(upload_to='store')
    video_block_id  = models.IntegerField()


class ClientOption(models.Model):
    message_types = (
                    ('MESSAGE','Message format (Just a string)'),
                    ('OPTIONS','Detailed JSON format containing options for client'),
    )
    name            = models.CharField(max_length=50)
    description     = models.CharField(max_length=100)
    details         = models.CharField(max_length=250)
    function        = models.CharField(max_length=100)
    messageType     = models.CharField(max_length=100, choices=message_types)
    stateType       = models.CharField(max_length=1, choices=ConnectionType.connection_types)
