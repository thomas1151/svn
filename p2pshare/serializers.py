from django.contrib.auth.models import User, Group
from p2pshare.models import Peer,System,Video
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class SystemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model  = System
        fields = ('connected', 'peer_ip')

class PeerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Peer
        fields = (  'peer_type','display_name','username','public_key','password')

class VideoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Video
        fields= ('store_date','store_length','length','name','uploader_id','x_resolution','y_resolution'
                    )
