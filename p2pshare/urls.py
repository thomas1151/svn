from django.conf.urls import url, include

from . import views
from django.contrib.auth.models import User
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'peers', views.PeerViewSet)
router.register(r'system', views.SystemViewSet)
router.register(r'video', views.VideoViewSet)


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^api/', include(router.urls)),

    #Customers
    # url(r'^customers/(?P<customer>\d+)/$', views.CustomerView.Single, name='customer'),

    url(r'^connection/$', views.ConnectionView.as_view(), name='connection'),
    url(r'^system/$', views.SystemManage.as_view()),


]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
